#version 460

uniform sampler2D texture;

in vec3 texCoord;
in vec4 position;
in vec3 pixelNormal;


layout (location=0) out vec4 fragColour;

vec3 calcLightContributionForPointLight(vec3 vertexPos, vec3 vertexNormal, vec3 L, vec3 Lcolour, vec3 Lk) {

  vec3 D = L - vec3(position.xyz);
  float lengthD = length(D);
  vec3 D_ = normalize(D);

  // Attenuation based on distance value lengthD
  float a = 1.0/(Lk.x + Lk.y*lengthD + Lk.z*lengthD*lengthD);

  // Lambertian
  float lambertian = clamp(dot(D_, pixelNormal), 0.0, 1.0);

  // Return final brightness for light source L
  return lambertian * a * Lcolour;

}

void main(void) {

	vec3 L[3];

	L[0] = vec3(2.0, 1.0, 0.0);
	L[1] = vec3(-3.0, 1.0, 0.0);
	L[2] = vec3(0.0, 2.0, 2.0);

	vec3 Lcol[3];
	Lcol[0] = vec3(0.0, 1.0, 0.0);
	Lcol[1] = vec3(0.0, 0.0, 1.0);
	Lcol[2] = vec3(1.0, 0.0, 0.0);

	vec3 LK[3];
	LK[0] = vec3(1.0, 0.1, 0.0);
	LK[1] = vec3(1.0, 0.1, 0.0);
	LK[2] = vec3(1.0, 0.1, 0.0);
	
	vec3 brightness = vec3(0.0, 0.0, 0.0);
	for (int i=0; i<2; i++){
	brightness += calcLightContributionForPointLight(position.xyz, pixelNormal, L[i], Lcol[i], LK[i]);
	}

	vec4 texColor = texture2D(texture, texCoord.xy);
	fragColour = texColor;
    
}