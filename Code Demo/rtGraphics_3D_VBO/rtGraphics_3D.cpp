// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <glew/glew.h>
#include <glew/wglew.h>
#include <GL\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "src/CGTexturedQuad.h"
#include "src/aiWrapper.h"
#include "src/CGPrincipleAxes.h"
#include "src/texture_loader.h"
#include "src/sceneVBOs.h"
#include "src/GURiffModel.h"
#include "al.h"
#include "alc.h"
#include "efx.h"
#include "EFX-Util.h"
#include "efx-creative.h"
#include <xram.h>
#include <mmreg.h>

using namespace std;
using namespace CoreStructures;

#pragma region Scene variables and resources

// Variables needed to track where the mouse pointer is so we can determine which direction it's moving in
int								mouse_x, mouse_y;
bool							mDown = false;
bool							wKeyDown, sKeyDown, aKeyDown, dKeyDown, qKeyDown, eKeyDown = false;

GUClock* mainClock = nullptr;

//
// Main scene resources
//
GUPivotCamera* mainCamera = nullptr;
CGPrincipleAxes* principleAxes = nullptr;
CGTexturedQuad* texturedQuad = nullptr;
//const aiScene* aiBeast;
const aiScene* aiTank;
const aiScene* aiBridge;
const aiScene* aiGrass;

GLuint tankTexture;
GLuint bridgeTexture;
GLuint GrassTexture;


sceneVBOs* Tank;
sceneVBOs* Bridge;
sceneVBOs* Grass; //vbo scenes used for imported models 
GLuint meshShader;

//Player position
float playerX = 4.0;
float playerY = 50.0;
float playerZ = 5.0;
#pragma endregion


#pragma region Function Prototypes

void init(int argc, char* argv[]); // Main scene initialisation function
void update(void); // Main scene update function
void display(void); // Main scene render function

// Event handling functions
void mouseButtonDown(int button_id, int state, int x, int y);
void mouseMove(int x, int y);
void mouseWheel(int wheel, int direction, int x, int y);
void keyDown(unsigned char key, int x, int y);
void closeWindow(void);
void reportContextVersion(void);
void reportExtensions(void);

#pragma endregion


int main(int argc, char* argv[])
{
	init(argc, argv);
	glutMainLoop();

	// Stop clock and report final timing data
	if (mainClock) {

		mainClock->stop();
		mainClock->reportTimingData();
		mainClock->release();
	}

	return 0;
}


void init(int argc, char* argv[]) {

	// Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE/* | GLUT_MULTISAMPLE*/);
	glutSetOption(GLUT_MULTISAMPLE, 4);

	// Setup window
	int windowWidth = 800;
	int windowHeight = 600;
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("CIS5013");
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Register callback functions
	glutIdleFunc(update); // Main scene update function
	glutDisplayFunc(display); // Main render function
	glutKeyboardFunc(keyDown); // Key down handler
	glutMouseFunc(mouseButtonDown); // Mouse button handler
	glutMotionFunc(mouseMove); // Mouse move handler
	glutMouseWheelFunc(mouseWheel); // Mouse wheel event handler
	glutCloseFunc(closeWindow); // Main resource cleanup handler


	// Initialise glew
	glewInit();

	// Initialise OpenGL...

	wglSwapIntervalEXT(0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW); // Default anyway

	// Setup colour to clear the window
	glClearColor(225.0f, 225.0f, 225.0f, 225.0f);//changed to a white background to allow transparency to look cleaner 

	// Setup main camera
	float viewportAspect = (float)windowWidth / (float)windowHeight;
	mainCamera = new GUPivotCamera(-40.0f, 1000.0f, 100.0f, 20.0f, viewportAspect, 0.5f);//camera position 

	principleAxes = new CGPrincipleAxes();

	texturedQuad = new CGTexturedQuad("..\\rtGraphics_3D_VBO\\Textures\\transparenecy.png");


	aiTank = aiImportModel(string("Models\\Tiger.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Tank = new sceneVBOs(aiTank);
	tankTexture = fiLoadTexture("..\\rtGraphics_3D_VBO\\Textures\\tigertex.png", TextureProperties(false));

	aiBridge = aiImportModel(string("Models\\bridge.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Bridge = new sceneVBOs(aiBridge);
	bridgeTexture = fiLoadTexture("..\\rtGraphics_3D_VBO\\Textures\\road.png", TextureProperties(false));

	aiGrass = aiImportModel(string("Models\\Grass.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Grass = new sceneVBOs(aiGrass);
	GrassTexture = fiLoadTexture("..\\rtGraphics_3D_VBO\\Textures\\Grass.png", TextureProperties(false));

	meshShader = setupShaders(string("shaders\\mesh_shader.vs"), string("shaders\\mesh_shader.fs"));

	//////Sound setup///
	ALCdevice* alcDevice = alcOpenDevice(NULL);
	ALCcontext* alcContext = nullptr;

	alcContext = alcCreateContext(alcDevice, NULL);
	alcMakeContextCurrent(alcContext);
	
	ALuint Mybuffer;
	alGenBuffers(1, &Mybuffer);
	
	auto myRIFFSound = new GURiffModel("..\\rtGraphics_3D_VBO\\Audio\\Theme.wav");

	RiffChunk formatChunk = myRIFFSound->riffChunkForKey('tmf');
	RiffChunk dataChunk = myRIFFSound->riffChunkForKey('atad');

	WAVEFORMATEXTENSIBLE wv;
	memcpy_s(&wv, sizeof(WAVEFORMATEXTENSIBLE), formatChunk.data, formatChunk.chunkSize);

	alBufferData(
		Mybuffer,
		AL_FORMAT_MONO16,
		(ALvoid*)dataChunk.data,
		(ALsizei)dataChunk.chunkSize,
		(ALsizei)wv.Format.nSamplesPerSec
	);

	ALuint source1;

	alGenSources(1, &source1);

	//Attach buffer1 to source1
	alSourcei(source1, AL_BUFFER, Mybuffer);

	alSource3f(source1, AL_POSITION, 10.0F, 0.0F, 0.0F);
	alSource3f(source1, AL_VELOCITY, 0.0f, 0.0f, 0.0f);//change these bits
	alSource3f(source1, AL_DIRECTION, 0.0f, 0.0f, 0.0f);

	auto cameraLocation = mainCamera->cameraLocation();
	ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };
	ALfloat listenerOri[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };

	alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));
	alListenerfv(AL_VELOCITY, listenerVel);
	alListenerfv(AL_ORIENTATION, listenerOri);

	alSourcePlay(source1);

	// Setup and start the main clock
	mainClock = new GUClock();
}

// Main scene update function (called by FreeGLUT's main event loop every frame) 
void update(void) {

	// Update clock
	mainClock->tick();

	// Redraw the screen
	display();

	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "CIS5013. Average fps: %.0f; Average spf: %f", mainClock->averageFPS(), mainClock->averageSPF() / 1000.0f);
	glutSetWindowTitle(timingString);

	if (wKeyDown == true) {
		playerX = 0.3f + playerX; //change speed of tank going forward
		wKeyDown = false;
	}
	if (sKeyDown == true) {
		playerX = -0.3 + playerX; //change speed of tank going back 
		sKeyDown = false;
	}
	if (aKeyDown == true) {
		playerZ = -0.7 + playerZ;
		aKeyDown = false;
	}
	if (dKeyDown == true) {
		playerZ = 0.7 + playerZ;
		dKeyDown = false;
	}
	if (qKeyDown == true) {
		playerY = 0.1 + playerY;
		qKeyDown = false;
	}
	if (eKeyDown == true) {
		playerY = -0.1 + playerY;
		eKeyDown = false;
	}
}


void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set viewport to the client area of the current window
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	// Get view-projection transform as a GUMatrix4
	GUMatrix4 T = mainCamera->projectionTransform() * mainCamera->viewTransform() * GUMatrix4::translationMatrix(-playerX, -playerY, -playerZ);

	if (principleAxes)
		principleAxes->render(T);

	if (texturedQuad)
		texturedQuad->render(T);

	glEnable(GL_TEXTURE_2D);
	static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));


	glBindTexture(GL_TEXTURE_2D, tankTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 modelMatrix = GUMatrix4::translationMatrix(playerX, playerY, playerZ) * GUMatrix4::scaleMatrix(0.005, 0.005, 0.005) * GUMatrix4::rotationMatrix(-0.01, 0.0, 0.0);
	GUMatrix4 newT = T * modelMatrix;
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Tank->render();


	GUMatrix4 modelMatrix1 = GUMatrix4::translationMatrix(100.0, 50.0, 4.0) * GUMatrix4::scaleMatrix(0.005, 0.005, 0.005) * GUMatrix4::rotationMatrix(0.0, 3.14, 0.0);
	GUMatrix4 newT1 = T * modelMatrix1;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT1.M));
	Tank->render();

	glBindTexture(GL_TEXTURE_2D, bridgeTexture);
	glEnable(GL_TEXTURE_2D);
	modelMatrix = GUMatrix4::translationMatrix(5.0, 30, -1.0) * GUMatrix4::scaleMatrix(10.0, 10.0, 10.0) * GUMatrix4::rotationMatrix(0.0, 0.0, 0.0);
	newT = T * modelMatrix;
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Bridge->render();

	glBindTexture(GL_TEXTURE_2D, GrassTexture);
	glEnable(GL_TEXTURE_2D);
	modelMatrix = GUMatrix4::translationMatrix(5.0, 30, -1.0) * GUMatrix4::scaleMatrix(1.0, 1.0, 1.0) * GUMatrix4::rotationMatrix(-190, 0.0, 0.01);
	newT = T * modelMatrix;
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Grass->render();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	GUMatrix4 modelMatrix2 = GUMatrix4::translationMatrix(5.0, 50.0, -1.0) * GUMatrix4::scaleMatrix(1000.0, 1000.0, 1000.0)* GUMatrix4::rotationMatrix(-190, 0.0, 0.0);
	GUMatrix4 newT2 = T * modelMatrix2;
	texturedQuad->render(newT2);

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);


	glutSwapBuffers();
}



#pragma region Event handling functions

void mouseButtonDown(int button_id, int state, int x, int y) {

	if (button_id == GLUT_LEFT_BUTTON) {

		if (state == GLUT_DOWN) {

			mouse_x = x;
			mouse_y = y;

			mDown = true;

		}
		else if (state == GLUT_UP) {

			mDown = false;
		}
	}
}


void mouseMove(int x, int y) {

	int dx = x - mouse_x;
	int dy = y - mouse_y;

	if (mainCamera) {
		mainCamera->transformCamera((float)-dy, (float)-dx, 0.0f);

		auto cameraLocation = mainCamera->cameraLocation();
		alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));

		cameraLocation.normalise();
		ALfloat orientaion[] = { -cameraLocation.x, -cameraLocation.y, -cameraLocation.z, 0.0f, 1.0f, 0.0f };
		alListenerfv(AL_ORIENTATION, orientaion); //allows for more 3D feel when moving around the scene
	}
	mouse_x = x;
	mouse_y = y;
}


void mouseWheel(int wheel, int direction, int x, int y) {

	if (mainCamera) {

		if (direction < 0)
			mainCamera->scaleCameraRadius(1.1f);
		else if (direction > 0)
			mainCamera->scaleCameraRadius(0.9f);

		auto cameraLocation = mainCamera->cameraLocation();
		alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));

		cameraLocation.normalise();
		ALfloat orientaion[] = { -cameraLocation.x, -cameraLocation.y, -cameraLocation.z, 0.0f, 1.0f, 0.0f };
		alListenerfv(AL_ORIENTATION, orientaion); //allows for more 3D feel when zooming in and out of the scene
	}
}


void keyDown(unsigned char key, int x, int y) {

	// Toggle fullscreen (This does not adjust the display mode however!)
	if (key == 'f')
		glutFullScreenToggle();
	if (key == 'w')
		wKeyDown = true;
	if (key == 's')
		sKeyDown = true;
	if (key == 'a')
		aKeyDown = true;
	if (key == 'd')
		dKeyDown = true;
	if (key == 'q')
		qKeyDown = true;
	if (key == 'e')
		eKeyDown = true;

}


void closeWindow(void) {

	// Clean-up scene resources

	if (mainCamera)
		mainCamera->release();

	if (principleAxes)
		principleAxes->release();

	if (texturedQuad)
		texturedQuad->release();

	//if (exampleModel)
		//exampleModel->release();
}


#pragma region Helper Functions

void reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char* glExtensionString = (const char*)glGetString(GL_EXTENSIONS);

	char* strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char* sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}

#pragma endregion


#pragma endregion

